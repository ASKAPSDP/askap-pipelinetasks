/// @file CModelWorker.cc
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Ben Humphreys <ben.humphreys@csiro.au>

// Include own header file first
#include "askap/pipelinetasks/cmodel/CModelWorker.h"

// Include package level header file
#include "askap_pipelinetasks.h"

// ASKAPsoft includes
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "boost/scoped_ptr.hpp"
#include "Common/ParameterSet.h"

// Casacore includes
#include "casacore/casa/aipstype.h"
#include "casacore/images/Images/TempImage.h"
#include "casacore/images/Images/PagedImage.h"

// Local package includes
#include "askap/pipelinetasks/cmodel/MPIBasicComms.h"
#include "askap/pipelinetasks/cmodel/ComponentImagerWrapper.h"
#include "askap/pipelinetasks/cmodel/ImageFactory.h"

// Using
using namespace askap;
using namespace askap::cp::pipelinetasks;
using namespace askap::cp::sms::client;
using namespace casacore;

ASKAP_LOGGER(logger, ".CModelWorker");

CModelWorker::CModelWorker(MPIBasicComms& comms)
    : itsComms(comms)
{
}

void CModelWorker::run(void)
{
    // Parameter set
    LOFAR::ParameterSet parset;

    // Obtain the parset via broadcast
    itsComms.broadcastParset(parset, 0);

    std::string filename = parset.getString("filename","");
    if ( filename != "" ) {
        // How many terms to handle?
        const unsigned int nterms = parset.getUint("nterms", 1);

        for (unsigned int term = 0; term < nterms; ++term) {
            // Create a TempImage and the component imager
            casacore::TempImage<casacore::Float> image = ImageFactory::createTempImage(parset);
            ComponentImagerWrapper imager(parset);

            // Signal master ready, receive and image components until the master
            // signals completion by sending an empty list
            std::vector<askap::cp::sms::client::Component> list;
            do {
                itsComms.signalReady(0);
                list = itsComms.receiveComponents(0);

                ASKAPLOG_DEBUG_STR(logger, "Imaging list of " << list.size() << " components");
                imager.projectComponents(list, image, term);
            } while (!list.empty());

            ASKAPLOG_DEBUG_STR(logger, "Beginning reduction");
            itsComms.sumImages(image, 0);
            ASKAPLOG_DEBUG_STR(logger, "Reduction complete");
        }
    } else {
        // no filename parameter in the parset so no image is produced. Worker has nothing to do here
        ASKAPLOG_WARN_STR(logger, "There is no filename parameter in the parset. Therefore, CModel does not generate an image");
    }
}
