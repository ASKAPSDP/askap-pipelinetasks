/// @file ComponentsAdaptor.cc
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>


// ASKAPsoft includes
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapUtil.h"

// Local package includes
#include "askap/pipelinetasks/cmodel/NonSkyModelComponentsAdaptor.h"


using namespace askap::cp::pipelinetasks;
using namespace askap::cp::sms::client;
using namespace askap;
using namespace casacore;

ASKAP_LOGGER(logger, ".NonSkyModelComponentsAdaptor");

NonSkyModelComponentsAdaptor::NonSkyModelComponentsAdaptor(const LOFAR::ParameterSet& parset)
{
    if ( parset.isDefined("primarybeam") ) {
        // dont create a primary beam if it is not set
        itsPbModel = askap::imagemath::PrimaryBeamFactory::make(parset);
    }

    // Get the centre of the image
    const std::vector<std::string> dirVector = parset.getStringVector("direction");
    const MDirection mdir = asMDirection(dirVector);
    itsCentreRA = mdir.getValue().getLong("deg");
    itsCentreDec = mdir.getValue().getLat("deg");
    // Get the flux limit
    const std::string fluxLimitStr = parset.getString("flux_limit");
    itsFluxLimit = asQuantity(fluxLimitStr, "Jy");
}


void
NonSkyModelComponentsAdaptor::calFluxAtCmodelDesiredFreq(askap::cp::sms::client::ComponentListPtr componentListPtr)
{
    // nothing to do
}
