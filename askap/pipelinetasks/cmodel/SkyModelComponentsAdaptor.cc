/// @file ComponentsAdaptor.cc
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#include <fstream>

// ASKAPsoft includes
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapUtil.h"

// Local package includes
#include "askap/pipelinetasks/cmodel/SkyModelComponentsAdaptor.h"

#include <iomanip>

using namespace askap::cp::pipelinetasks;
using namespace askap::cp::sms::client;
using namespace askap;
using namespace casacore;

ASKAP_LOGGER(logger, ".SkyModelComponentsAdaptor");

SkyModelComponentsAdaptor::SkyModelComponentsAdaptor(const LOFAR::ParameterSet& parset)
{
    if ( parset.isDefined("primarybeam") ) { 
        // dont create a primary beam if it is not set
        itsPbModel = askap::imagemath::PrimaryBeamFactory::make(parset);
    }

    if ( !parset.isDefined("forceSpectralProperty") ) {
        ASKAPTHROW(AskapError, "To Use SkyModel service, Cmodel must set forceSpectralProperty=true");
    }
    bool forceSpectralProperty = parset.getBool("forceSpectralProperty",false);
    ASKAPCHECK(forceSpectralProperty != false,"To Use SkyModel service, Cmodel must set forceSpectralProperty=true");

    // To use sms, the user must provide a value for alpha (spectral index)
    // and beta (spectral curvature) for now. However, once the catalogue
    // has these values and ingested to the sms database, these values probably
    // have to be removed from the code.
    itsAlpha = parset.getDouble("userDefinedSpectralIndex",0.0);
    itsBeta = parset.getDouble("userDefinedSpectralCurvature",0.0);
    itsCmodelFreq = asQuantity(parset.getString("frequency"),"Hz").getValue("Hz");
    itsRefFreq = asQuantity(parset.getString("gsm.ref_freq"),"Hz").getValue("Hz");

    // Get the centre of the image
    const std::vector<std::string> dirVector = parset.getStringVector("direction");
    const MDirection mdir = asMDirection(dirVector);
    itsCentreRA = mdir.getValue().getLong("deg");
    itsCentreDec = mdir.getValue().getLat("deg");
    // Get the flux limit
    const std::string fluxLimitStr = parset.getString("flux_limit");
    itsFluxLimit = asQuantity(fluxLimitStr, "Jy");
}

void
SkyModelComponentsAdaptor::calFluxAtCmodelDesiredFreq(askap::cp::sms::client::ComponentListPtr componentListPtr)
{
    ASKAPCHECK(componentListPtr.get() != nullptr, "ComponentListPtr object is NULL");
    ComponentList& components = *componentListPtr;
    ASKAPCHECK(itsRefFreq != 0.0, "Cmodel.gsm.ref_freq parameter is eqaul to 0");
    ASKAPCHECK( (itsCmodelFreq/itsRefFreq) > 0.0, "Cmodel.frequency or Cmodel.gsm.ref_freq parameters must be greater than 0");
    for ( askap::cp::sms::client::Component& c : components ) {
        const double alphaNew = itsAlpha + (itsBeta * log(itsCmodelFreq/itsRefFreq));
        // The formula is taken from here :
        // https://www.atnf.csiro.au/computing/software/askapsoft/sdp/docs/current/analysis/postprocessing.html#spectral-index-curvature
        const double temp = std::pow(itsCmodelFreq/itsRefFreq,alphaNew);
        casacore::Quantity newFlux = c.flux() * temp;
        ASKAPLOG_DEBUG_STR(logger,"At Ref Freq: " << itsRefFreq << ", flux = " << c.flux().getValue("mJy")
                                << ". At Freq: " << itsCmodelFreq << ", new flux = " << newFlux.getValue("mJy")
                                << ". alpha = " << itsAlpha << ". beta = " << itsBeta);
        // update the component with the new flux i.e flux at the observing frequency
        c.flux(newFlux);
        // for now, update the component with the new spectral index value.
        // the statement below will be removed once the RACS catalogue has
        // the spectral index value in it.
        c.spectralIndex(alphaNew);
    }
}
