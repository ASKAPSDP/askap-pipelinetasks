/// @file CModelMaster.cc
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Ben Humphreys <ben.humphreys@csiro.au>

// Include own header file first
#include "askap/pipelinetasks/cmodel/CModelMaster.h"

// Include package level header file
#include "askap_pipelinetasks.h"

// System includes
#include <cmath>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <iomanip>

// ASKAPsoft includes
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapUtil.h"

// Local package includes
#include "askap/pipelinetasks/cmodel/VOTableAccessor.h"
#include "askap/pipelinetasks/cmodel/AsciiTableAccessor.h"
#include "askap/pipelinetasks/cmodel/DataserviceAccessor.h"
#include "askap/pipelinetasks/cmodel/MPIBasicComms.h"
#include "askap/pipelinetasks/cmodel/ImageFactory.h"
#include "askap/imagemath/primarybeam/PrimaryBeamFactory.h"
#include "askap/pipelinetasks/cmodel/GSMFactory.h"
#include "askap/pipelinetasks/cmodel/SkyModelComponentsAdaptor.h"
#include "askap/pipelinetasks/cmodel/NonSkyModelComponentsAdaptor.h"

// Using
using namespace askap;
using namespace askap::cp::pipelinetasks;
using namespace askap::cp::sms::client;
using namespace casacore;

ASKAP_LOGGER(logger, ".CModelMaster");

CModelMaster::CModelMaster(const LOFAR::ParameterSet& itsParset, MPIBasicComms& comms)
        : itsParset(itsParset), itsComms(comms)
{
}

void CModelMaster::run(void)
{
    ASKAPLOG_INFO_STR(logger, "Running master");
    // Broadcast the parset
    LOFAR::ParameterSet parset = itsParset; // Avoid const_cast
    itsComms.broadcastParset(parset, 0);

    // Interface to GSM data
    //boost::scoped_ptr<IGlobalSkyModel> gsm;
    std::unique_ptr<IGlobalSkyModel> gsm = GSMFactory::make(itsParset);

    // Setup the GSM accessor
    itsCompAdaptor = GSMFactory::makeComponentsAdaptor(itsParset);

    ASKAPCHECK(gsm.get() != nullptr, "gsm is a null ptr");
    //ComponentListPtr pList = gsm->coneSearch(ra, dec, searchRadius, fluxLimit);
    ComponentListPtr pList = GSMFactory::coneSearch(gsm,itsParset);
    gsm.reset();
    ASKAPCHECK(itsCompAdaptor.get() != nullptr, "Component Adaptor object is NULL");

    itsCompAdaptor->calFluxAtCmodelDesiredFreq(pList);
    // apply the taper function
    ASKAPLOG_INFO_STR(logger, "Number of components in result set prior to taper " << pList->size());
    primaryBeamTaper(pList);
    ASKAPLOG_INFO_STR(logger, "Number of components in result set after taper " << pList->size());

    if ( ! pList->empty() ) {
        toParsetFormat(pList);
    } else {
        ASKAPLOG_WARN_STR(logger,"There are no components in the list.");
    }

    // Create an image and sum all workers images to the master only if the filename parameter
    // is defined in the parset
    std::string fname = parset.getString("filename","");
    if ( fname != "" ) {
        const casacore::uInt batchSize = itsParset.getUint("batchsize", 200);
        const unsigned int nterms = itsParset.getUint("nterms", 1);

        // Send components to each worker until complete
        for (unsigned int term = 0; term < nterms; ++term) {
            if (nterms > 1) {
                ASKAPLOG_INFO_STR(logger, "Imaging taylor term " << term);
            }
            size_t idx = 0;
            std::vector<askap::cp::sms::client::Component> subset;
            while (idx < pList->size()) {
                // Get a batch ready - an "nelement" subset of "pList-> will be sent
                const size_t nelements = (idx + batchSize < pList->size()) ? batchSize : (pList->size() - idx);
                subset.assign(pList->begin() + idx, pList->begin() + idx + nelements);
                idx += nelements;

                // Wait for a worker to become available
                const int worker = itsComms.getReadyWorkerId();
                ASKAPLOG_DEBUG_STR(logger, "Allocating " << subset.size()
                    << " components to worker " << worker);
                itsComms.sendComponents(subset, worker);
                ASKAPLOG_INFO_STR(logger, "Master has allocated " << idx << " of "
                    << pList->size() << " components");
            }

            // Send each worker an empty list to signal completion, need to first consume
            // the ready signals so the workers will unblock.
            subset.clear();
            for (int i = 1; i < itsComms.getNumNodes(); ++i) {
                const int worker = itsComms.getReadyWorkerId();
                itsComms.sendComponents(subset, worker);
            }

            // Create an image and sum all workers images to the master
            std::string filename = parset.getString("filename");
            if (nterms > 1) {
                filename += ".taylor.";
                filename += askap::utility::toString(term);
            }

            casacore::PagedImage<casacore::Float> image = ImageFactory::createPagedImage(parset, filename);
            ASKAPLOG_INFO_STR(logger, "Beginning reduction step");
            itsComms.sumImages(image, 0);
            ASKAPLOG_INFO_STR(logger, "Completed reduction step");
        }
    } else {
        ASKAPLOG_WARN_STR(logger, "There is no filename parameter in the parset. Therefore, CModel does not generate an image");
    }
}

void
CModelMaster::toParsetFormat(askap::cp::sms::client::ComponentListPtr componentListPtr,
                            const std::string& prefix)
{
    const std::string fname = itsParset.getString("components.outfile","");
    if ( fname == "" ) {
        ASKAPLOG_WARN_STR(logger, "components.outfile parameter is not defined in parset so components are not written to file");
        return;
    }


    ASKAPCHECK(componentListPtr.get() != nullptr,"ComponentListPtr is null");
    if ( itsParset.isDefined("sources.names") ) {
        LOFAR::ParameterSet ps;
        std::ofstream ofile;
        ofile.open(fname,std::ios::trunc);
        if ( ofile ) {
            ofile.setf(std::ios::left);
            const std::vector<std::string> srcNames = itsParset.getStringVector("sources.names");
            ASKAPCHECK(srcNames.size() == 1, "sources.names parameter should have only 1 name");
            std::string srcName = srcNames[0];
            const std::vector<std::string> dirVect = itsParset.getStringVector("direction");
            ASKAPCHECK(dirVect.size() == 3, "direction parameter does not contain RA, Dec, Epoch");
            const unsigned int nterms = itsParset.getUint("nterms", 1);
            ofile << std::setw(50) << prefix + "sources.names" <<  std::setw(5) << "=" << std::setw(20) << "[" + srcName + "]" << std::endl;
            ofile << std::setw(50) << prefix + "sources." + srcName + ".direction" << std::setw(5) << "=" << std::setw(50) << "[" + dirVect[0] + ", " + dirVect[1] + ", " + dirVect[2] + "]" << std::endl;
            ofile << std::setw(50) << prefix + "sources." + srcName + ".nterms" << std::setw(5) <<  "=" << std::setw(5) << std::to_string(nterms) << std::endl;
            askap::cp::sms::client::ComponentList& components = *componentListPtr;
            unsigned long index = 0;
            const unsigned long numOfComponents = components.size();
            long id = 0;
            ofile << std::setw(50) << prefix + "sources." + srcName + ".components" <<  std::setw(5) << "=" << "[";
            for ( const askap::cp::sms::client::Component& c : components ) {
                if ( index != numOfComponents - 1 ) {
                    // This is ugly because the id generated using the votable (i.e not by sms) is always -1
                    if ( c.id() == -1 ) {
                        ofile << "component-" << id << ",";
                        id += 1;
                    } else {
                        ofile << "component-" << c.id() << ",";
                    }
                } else {
                    if ( c.id() == -1 ) {
                        ofile << "component-" << id << "]" << std::endl;
                        id += 1;
                    } else {
                        ofile << "component-" << c.id() << "]" << std::endl;
                    }
                }
                index += 1;
            }
            id = 0;
            const MDirection mdir = asMDirection(dirVect);
            const double refRA = mdir.getValue().getLong(); // in radians
            const double refDec = mdir.getValue().getLat(); // in radians
            const double refFreq = asQuantity(itsParset.getString("gsm.ref_freq"),"Hz").getValue("MHz");
            for ( const askap::cp::sms::client::Component& c : components ) {
                ofile << std::endl;
                const double srcRA = c.rightAscension().getValue("rad");
                const double srcDec = c.declination().getValue("rad");
                // this is copy from the askap-analysis package of the ParsetComponent class. The offset calculation is defined in the defineComponent() method
                double raOffset = sin(srcRA-refRA) * cos(srcDec);
                double decOffset = sin(srcDec) * cos(refDec) - cos(srcDec) * sin(refDec) * cos(srcRA - refRA);
                // This is ugly because the id generated using the votable (i.e not by sms) is always -1
                if ( c.id() == -1 ) {
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".flux.i" << std::setw(5) << "=" << c.flux() << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".flux.spectral_index" << std::setw(5) << "=" << c.spectralIndex() << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".flux.ref_freq" << std::setw(5) << "=" << refFreq  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".direction.ra" << std::setw(5) << "=" << raOffset  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".direction.dec" << std::setw(5) << "=" << decOffset  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".shape.bmaj" << std::setw(5) << "=" << c.majorAxis().getValue("rad")  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".shape.bmin" << std::setw(5) << "=" << c.minorAxis().getValue("rad")  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(id) + ".shape.bpa" << std::setw(5) << "=" << c.positionAngle().getValue("rad")  << std::endl;
                    id += 1;
                } else {
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".flux.i" << std::setw(5) << "=" << c.flux() << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".flux.spectral_index" << std::setw(5) << "=" << c.spectralIndex() << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".flux.ref_freq" << std::setw(5) << "=" << refFreq  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".direction.ra" << std::setw(5) << "=" << raOffset  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".direction.dec" << std::setw(5) << "=" << decOffset  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".shape.bmaj" << std::setw(5) << "=" << c.majorAxis().getValue("rad")  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".shape.bmin" << std::setw(5) << "=" << c.minorAxis().getValue("rad")  << std::endl;
                    ofile << std::setw(70) << prefix + "sources." + srcName + ".component-" + std::to_string(c.id()) + ".shape.bpa" << std::setw(5) << "=" << c.positionAngle().getValue("rad")  << std::endl;
                }
            }
            ofile.close();
        }

    }
}

void
CModelMaster::primaryBeamTaper(ComponentListPtr pList)
{
    const float dcOffset = itsParset.getFloat("dcoffset",0.0);
    askap::cp::sms::client::ComponentList& components = *pList; // typedef std::vector<askap::cp::sms::client::Component> ComponentList;
    for ( std::vector<askap::cp::sms::client::Component>::iterator it = components.begin(); it != components.end();) {
        if (itsCompAdaptor->process(*it,dcOffset)) {
            it = components.erase(it);
        } else {
            ++it;
        }
    }
}
