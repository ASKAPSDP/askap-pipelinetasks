/// @file ComponentsAdaptor.h
///
/// @copyright (c) 2017 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#ifndef ASKAP_CP_PIPELINETASKS_SMS_COMPONENT_ADAPTOR_H
#define ASKAP_CP_PIPELINETASKS_SMS_COMPONENT_ADAPTOR_H

#include "askap/pipelinetasks/cmodel/IComponentsAdaptor.h"
//
namespace askap {
namespace cp {
namespace pipelinetasks {
class SkyModelComponentsAdaptor : public IComponentsAdaptor {
    public :
        SkyModelComponentsAdaptor(const LOFAR::ParameterSet& parset);
        ~SkyModelComponentsAdaptor() {}

        /// @brief  This method calculates the flux at the observing frequency.
        /// @detail The flux returned from the skymodel service is the flux at
        ///           a reference frequency in the catalogue. The flux that cmodel
        ///           requests is the flux at the observing frequency and it is obtained
        ///           by using the formula flux_output = flux_ref * (freq_output/freq_ref)^-alpha
        /// where flux_ref = flux of the component at the reference GSM frequency.
        ///       freq_ref = frequency at which the GSM was generated.
        ///       freq_output = frequency at which user wants the GSM.
        ///       flux_output = flux of the component at the user-desired frequency freq_output`
        ///       note: To use sms, the user must provide a value for alpha (spectral index)
        ///       and beta (spectral curvature) for now. However, once the catalogue
        ///       has these values and ingested to the sms database, these values probably
        ///       have to be removed from the code and the values in the component will be
        ///       used in the calculation.
        /// @param [in/out] componentListPtr - a shared pointer to a vector of components
        void calFluxAtCmodelDesiredFreq(askap::cp::sms::client::ComponentListPtr componentListPtr) override;

    private :

        /// These may not be needed in the future
        /// To use sms, the user must provide a value for alpha (spectral index)
        /// and beta (spectral curvature) for now. However, once the catalogue
        /// has these values and ingested to the sms database, these values probably
        /// have to be removed from the code.
        double itsAlpha;
        double itsBeta;


        /// Reference frequency
        /// With the changes in ticket AXA-1868 (i.e the Component class now has
        /// a frequency field (the frequency at which the image/catalogue was made), 
        /// the member itsRefFreq is no longer required). For the time being, just keep 
        /// it here because the RACS catalogue/database currently does not have a frequency 
        /// field/value but aware that it may not be needed in the future.
        double itsRefFreq;
};
} // pipelinetasks
} // cp
} // askap

#endif
