/// @file GSMFactory.h
///
/// @copyright (c) 2017 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#ifndef ASKAP_CP_PIPELINETASKS_GSM_FACTORY_H
#define ASKAP_CP_PIPELINETASKS_GSM_FACTORY_H

// ASKAPsoft includes
#include "Common/ParameterSet.h"
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapUtil.h"
// Local package includes
#include "askap/pipelinetasks/cmodel/Common.h"
#include "askap/pipelinetasks/cmodel/VOTableAccessor.h"
#include "askap/pipelinetasks/cmodel/AsciiTableAccessor.h"
#include "askap/pipelinetasks/cmodel/DataserviceAccessor.h"
#include "askap/pipelinetasks/cmodel/IComponentsAdaptor.h"

#include <memory>

namespace askap {
namespace cp {
namespace pipelinetasks {
class GSMFactory {
    public:
        //static std::shared_ptr<IGlobalSkyModel> make(const LOFAR::ParameterSet& parset);
        //static std::shared_ptr<IComponentsAdaptor> makeComponentsAdaptor(const LOFAR::ParameterSet& parset);
        static std::unique_ptr<IGlobalSkyModel> make(const LOFAR::ParameterSet& parset);
        static std::unique_ptr<IComponentsAdaptor> makeComponentsAdaptor(const LOFAR::ParameterSet& parset);
        static askap::cp::sms::client::ComponentListPtr coneSearch(std::unique_ptr<IGlobalSkyModel>& gsm, const LOFAR::ParameterSet& parset);
};
} // pipelinetasks
} // cp
} // askap
#endif
