/// @file ComponentsAdaptor.cc
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

// ASKAPsoft includes
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapUtil.h"

// Local package includes
#include "askap/pipelinetasks/cmodel/IComponentsAdaptor.h"


using namespace askap::cp::pipelinetasks;
using namespace askap::cp::sms::client;
using namespace askap;
using namespace casacore;

ASKAP_LOGGER(logger, ".IComponentsAdaptor");

IComponentsAdaptor::~IComponentsAdaptor()
{
}

bool
IComponentsAdaptor::process(askap::cp::sms::client::Component& component, float dcOffset) const
{
    bool drop = true;

    const casacore::MVDirection compDir(component.rightAscension(),component.declination());
    const casacore::MVDirection refDir(itsCentreRA,itsCentreDec);
    double separation = refDir.separation(compDir);
    double posAngle = refDir.positionAngle(compDir); // assume in radians
    casacore::Quantity predictedFlux;
    float pbAlpha = 0.0;
    float pbBeta = 0.0;
    double taperFactor = 1.0;
    int applyPBM = 1;
    if ( itsPbModel != nullptr ) {
        taperFactor = itsPbModel->evaluateAtOffset(posAngle,separation,itsCmodelFreq);
        predictedFlux = component.flux() * std::max(taperFactor,static_cast<double>(dcOffset));
        itsPbModel->evaluateAlphaBetaAtOffset(pbAlpha,pbBeta,posAngle,separation,itsCmodelFreq);
    } else {
        // if primarybeam parameter is not set, dont calculate the attenuation value
        predictedFlux = component.flux();
        applyPBM = 0;
    }

    if ( predictedFlux > itsFluxLimit ) {
        // Update the flux in the component with the predicted flux
        const auto old_flux = component.flux();
        component.flux(predictedFlux);
        // Add the primary beam alpha (spectral index) to the component's spectral index
        // clip the spectral index to within [-99,99] range
        double si = pbAlpha + component.spectralIndex();
        if ( si >= 99.0 ) {
            si = 99.0;
        } else if ( si <= -99.0 ) {
            si = -99.0;
        }
        component.spectralIndex(si);
        drop = false;

        ASKAPLOG_DEBUG_STR(logger,"refDir: " << refDir
                            << ", compDir: " << compDir
                            << ", freq: " << itsCmodelFreq
                            << ", separation: " << separation
                            << ", posAngle: " << posAngle
                            << ", taperFactor: " << taperFactor
                            << ", applyPBM: " << applyPBM
                            << ", fluxLimit: " << itsFluxLimit
                            << ", predictedFlux (in mJy): " << predictedFlux
                            << ", old_flux (in mJy): " << old_flux
                            << ", component freq: " << component.frequency()
                            << ", spectral index: " << component.spectralIndex());

    }

    return drop;
}
