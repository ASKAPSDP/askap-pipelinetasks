/// @file GSMFactory.cc
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>


#include "askap/pipelinetasks/cmodel/GSMFactory.h"
#include "askap/pipelinetasks/cmodel/SkyModelComponentsAdaptor.h"
#include "askap/pipelinetasks/cmodel/NonSkyModelComponentsAdaptor.h"
#include "askap/askap/AskapLogging.h"


using namespace casacore;
using namespace askap::cp::pipelinetasks;

ASKAP_LOGGER(logger, ".GSMFactory");

std::unique_ptr<IGlobalSkyModel>
GSMFactory::make(const LOFAR::ParameterSet& parset)
{
    std::unique_ptr<IGlobalSkyModel> gsm;
    const std::string database = parset.getString("gsm.database");
    if (database == "votable") {
        const std::string filename = parset.getString("gsm.file");
        gsm.reset(new VOTableAccessor(filename));
    } else if (database == "asciitable") {
        const std::string filename = parset.getString("gsm.file");
        gsm.reset(new AsciiTableAccessor(filename, parset));
    } else if (database == "dataservice") {
        const std::string host = parset.getString("gsm.locator_host");
        const std::string port = parset.getString("gsm.locator_port");
        const std::string serviceName = parset.getString("gsm.service_name");
        const std::string adapterName = parset.getString("gsm.adapter_name");
        gsm.reset(new DataserviceAccessor(host, port, serviceName,adapterName));
    } else {
        ASKAPTHROW(AskapError, "Unknown GSM database type: " << database);
    }

    return gsm;
}

std::unique_ptr<IComponentsAdaptor>
GSMFactory::makeComponentsAdaptor(const LOFAR::ParameterSet& parset)
{
    std::unique_ptr<IComponentsAdaptor> adaptor;
    const std::string database = parset.getString("gsm.database");
    if (database == "votable" || database == "asciitable") {
        adaptor.reset(new NonSkyModelComponentsAdaptor(parset));
    } else if (database == "dataservice") {
        adaptor.reset(new SkyModelComponentsAdaptor(parset));
    } else {
        ASKAPTHROW(AskapError, "Unknown GSM database type: " << database);
    }

    return adaptor;
}

askap::cp::sms::client::ComponentListPtr
GSMFactory::coneSearch(std::unique_ptr<IGlobalSkyModel>& gsm, const LOFAR::ParameterSet& parset)
{
    ASKAPCHECK(gsm.get() != nullptr, "gsm is a null ptr");

    // Get the flux limit
    const std::string fluxLimitStr = parset.getString("flux_limit");
    const Quantity fluxLimit = asQuantity(fluxLimitStr, "Jy");

    // Get the centre of the image
    const std::vector<std::string> dirVector = parset.getStringVector("direction");
    const MDirection mdir = asMDirection(dirVector);
    const Quantum<Double> ra = mdir.getValue().getLong("deg");
    const Quantum<Double> dec = mdir.getValue().getLat("deg");

    bool calAnnulus = parset.getBool("extractFromAnnulus",false);
    if ( !calAnnulus ) {
        // Detrmine the search radius
        // At the moment just use the 1D size of the image multiplied by the
        // cellsize to determine the search radius. Because the dimensions or scale
        // may not be identical, use the larger of the two. This is almost 2x the
        // field, but given the current implementations of cone search do not
        // include extended components with centre outside the field, it is best
        // to search a larger radius anyway.
        const casacore::uInt nx = parset.getUintVector("shape").at(0);
        const casacore::uInt ny = parset.getUintVector("shape").at(1);
        const std::vector<std::string> cellSizeVector = parset.getStringVector("cellsize");
        const Quantity xcellsize = asQuantity(cellSizeVector.at(0), "arcsec");
        const Quantity ycellsize = asQuantity(cellSizeVector.at(1), "arcsec");
        const casacore::Quantity searchRadius(
            std::max(xcellsize.getValue("deg") * nx, ycellsize.getValue("deg") * ny),
            "deg");
        ASKAPLOG_INFO_STR(logger,"Radius in degrees: " << searchRadius);
        askap::cp::sms::client::ComponentListPtr pList = gsm->coneSearch(ra, dec, searchRadius, fluxLimit);

        return pList;
    } else {
        const std::vector<std::string> radiusVector = parset.getStringVector("Radii");
        ASKAPCHECK(radiusVector.size() != 0, "Cmodel.R parameter is not set");
        const Quantity innerRadius = asQuantity(radiusVector.at(0), "deg");
        const Quantity outerRadius = asQuantity(radiusVector.at(1), "deg");
        askap::cp::sms::client::ComponentListPtr innerComponentList = gsm->coneSearch(ra, dec, innerRadius, fluxLimit);
        askap::cp::sms::client::ComponentListPtr outerComponentList = gsm->coneSearch(ra, dec, outerRadius, fluxLimit);
        ASKAPLOG_INFO_STR(logger,"Size of inner components: " << innerComponentList->size());
        ASKAPLOG_INFO_STR(logger,"Size of outer components: " << outerComponentList->size());
        for ( const auto& c : *innerComponentList ) {
            ASKAPLOG_DEBUG_STR(logger,"Inner Components: " << c.id());
        }
        for ( const auto& c : *outerComponentList ) {
            ASKAPLOG_DEBUG_STR(logger,"Outer Components: " << c.id());
        }

        const std::vector<askap::cp::sms::client::Component>& innerCompList = *innerComponentList;
        const std::vector<askap::cp::sms::client::Component>& outerCompList = *outerComponentList;
        askap::cp::sms::client::ComponentListPtr annulusCompListPtr(new askap::cp::sms::client::ComponentList());
        // filter out the outer components list that are not in the inner components list and add
        // them to the annulus components list
        for ( const askap::cp::sms::client::Component& outerComp : outerCompList ) {
            if (std::none_of(innerCompList.begin(), innerCompList.end(),
                            [outerComp](const askap::cp::sms::client::Component& innerComp) {return innerComp.id() == outerComp.id();}) ) {
                annulusCompListPtr->push_back(outerComp);
            }
        }

        // sort the components in desending order of flux
        std::sort(annulusCompListPtr->begin(),annulusCompListPtr->end(),
                  [](const askap::cp::sms::client::Component& c1, const askap::cp::sms::client::Component& c2)
                  { return c2.flux() < c1.flux(); });

        ASKAPLOG_INFO_STR(logger,"Size of annulus components: " << annulusCompListPtr->size());
        for ( const auto& c : *annulusCompListPtr ) {
            ASKAPLOG_DEBUG_STR(logger,"Annulus Components: " << c.id() << ", flux: " << c.flux());
        }
        return annulusCompListPtr;
    }
}
