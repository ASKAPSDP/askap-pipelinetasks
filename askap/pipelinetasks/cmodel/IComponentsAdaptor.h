/// @file ComponentsAdaptor.h
///
/// @copyright (c) 2017 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#ifndef ASKAP_CP_PIPELINETASKS_ICOMPONENTS_ADAPTOR_H
#define ASKAP_CP_PIPELINETASKS_ICOMPONENTS_ADAPTOR_H

// System includes
#include <istream>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <memory>

// ASKAPsoft includes
#include "askap/askap/AskapError.h"
#include "boost/scoped_ptr.hpp"
#include "casacore/casa/aipstype.h"
#include "casacore/casa/Quanta/Quantum.h"
#include "casacore/casa/Quanta/Unit.h"
#include "Common/ParameterSet.h"
#include "askap/imagemath/primarybeam/PrimaryBeamFactory.h"
#include "askap/pipelinetasks/cmodel/Common.h"
//
namespace askap {
namespace cp {
namespace pipelinetasks {
/// @details - This is an abstract class. It defines a set of abstract methods
/// to be implemented by subclasses to modify/filter the components returned from
/// the cone search operation.
class IComponentsAdaptor {
    public :
        /// Constructor
        //IComponentsAdaptor(const LOFAR::ParameterSet& parset);
        /// Destructor
        virtual ~IComponentsAdaptor();

        /// @rief This method applies the beam model to the skymodel component to get the
        ///        expected measures sky response.
        /// @param [in] component - a skymododel component 
        /// @param [in] dcOffset - an offset added to the beam taper factor 
        /// @return true if the flux of the component is less than the flux limit after applying the
        ///         taper function and false otherwise. 
        bool process(askap::cp::sms::client::Component& component, float dcOffset) const; 

        /// @brief  This method calculates the flux at the observing frequency for each component in the components list.
        /// @param [in/out] componentListPtr - a shared pointer to a vector of components.
        virtual void calFluxAtCmodelDesiredFreq(askap::cp::sms::client::ComponentListPtr componentListPtr) = 0;

        boost::shared_ptr<askap::imagemath::PrimaryBeam> itsPbModel;

        /// Observing frequency
        double itsCmodelFreq;

        casacore::Quantity itsCentreRA;  // in degrees
        casacore::Quantity itsCentreDec; // in degrees
        casacore::Quantity itsFluxLimit; // in Jy
};
} // pipelinetasks
} // cp
} // askap

#endif
