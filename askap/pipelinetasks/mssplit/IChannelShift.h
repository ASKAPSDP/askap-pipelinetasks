/// @file IchannelShift.h
///
/// @copyright (c) 2013 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#ifndef ASKAP_CP_ICHANNELSHIFT_H
#define ASKAP_CP_ICHANNELSHIFT_H

// Package level header file
#include "askap_pipelinetasks.h"

// System includes
#include <string>
#include <utility>
#include <vector>

// ASKAPsoft includes
#include "Common/ParameterSet.h"

#include "casacore/casa/aipstype.h"
#include "casacore/casa/Arrays/Matrix.h"
#include "casacore/casa/BasicSL/Complexfwd.h"
#include "casacore/measures/Measures/MFrequency.h"
#include "casacore/measures/Measures/MeasConvert.h"

namespace askap {
namespace cp {
namespace pipelinetasks {

/// @brief - This is an abstract class used for implementing the interpolation functionality
///          of the doppler correction.
class IChannelShift {
    public:
        /// @details - Given a set of input frequency (chanFreq) and input data (data),
        ///            this method interpolates the output data for a new set of output
        ///            frequency (baryChanFreq). Note: chanFreq and baryChanFreq must
        ///            be in the same frame.
        /// @param[in] chanFreq - the input MS frequencies in TOPO frame
        /// @param[in] baryChanFreq - the output frequencies in BARY/LSRK frame converted to TOPO frame
        /// @param[in] data - the visibilities data 
        /// @param[in] inflag - flags for the visibilities data
        /// @param[out] outputdata - interpolated (or shifted) input data (data)
        /// @param[out] outflags - output MS flag values
        virtual void interpolate(const std::vector<double>& chanFreq, 
                                 const casacore::Vector<double>& baryChanFreq,
                                 const casacore::Array<casacore::Complex>& data,
                                 const casacore::Array<casacore::Bool>& inflags,
                                 casacore::Array<casacore::Complex>& outdata,
                                 casacore::Array<casacore::Bool>& flags) = 0;
        virtual ~IChannelShift() {}
};
}
}
}
#endif
