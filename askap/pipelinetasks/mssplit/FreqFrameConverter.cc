/// @file FreqFrameConverter.h
///
/// @copyright (c) 2013 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>


// Package level header file
#include "askap_pipelinetasks.h"

// Include own header file
#include "askap/pipelinetasks/mssplit/NearestNeighbourShift.h"
#include "askap/pipelinetasks/mssplit/LinearShift.h"
#include "askap/pipelinetasks/mssplit/CubicShift.h"
#include "askap/pipelinetasks/mssplit/FreqFrameConverter.h"


// ASKAPsoft includes
#include "Common/ParameterSet.h"
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapLogging.h"
#include "casacore/casa/Arrays/Cube.h"
#include "casacore/casa/Quanta/MVTime.h"
#include "casacore/ms/MeasurementSets/MSColumns.h"
#include "casacore/casa/Quanta.h"
#include "casacore/casa/Quanta/Quantum.h"
#include <initializer_list>

ASKAP_LOGGER(logger, ".freqframeconverter");

using namespace askap;
using namespace askap::cp::pipelinetasks;

FreqFrameConverter::FreqFrameConverter(const LOFAR::ParameterSet &parset)
{
    const std::string method = parset.getString("interpolation","nearest");
    if ( method == "linear") {
        itsChanShift.reset(new LinearShift {});
    } else if ( method == "cubic") {
        itsChanShift.reset(new CubicShift {});
    } else {
        itsChanShift.reset(new NearestNeighbourShift {});
    }
}

/// @brief - This method does the doppler correction.
/// @param[in] ms - input measurement set
/// @param[in] startChan - the start channel
/// @param[in] endChan - end channel
/// @param[in] width - channel width. (width > 1 => channels averaging which is not yet supported)
/// @param[in] row - current row in the main table
/// @param[in] nRowsThisIteration - the number of rows to process
/// @param[in] spwId - spectral window id
/// @param[in] outputRefFrame - the output frame (i.e BARY or LSRK)
/// @param[in] indata - the visibilities data of nRowsThisIteration
/// @param[out] outflags - output MS flag values for nRowsThisIteration
 /// @param[out] outputdata - output MS visibilities data for nRowsThisIteration
void
FreqFrameConverter::dopplerShift(const LOFAR::ParameterSet& parset,
                                 const casacore::MeasurementSet& ms,
                                 const uint32_t startChan,
                                 const uint32_t endChan,
                                 const uint32_t width,
                                 const casacore::rownr_t row,
                                 const casacore::uInt nRowsThisIteration,
                                 const casacore::Int spwId,
                                 const casacore::MFrequency::Types outputRefFrame,
                                 const casacore::Array<casacore::Complex>& indata,
                                 casacore::Cube<casacore::Bool>& outflags,
                                 casacore::Cube<casacore::Complex>& outputdata)
{
    ASKAPLOG_DEBUG_STR(logger,"calling cvel");

    const casacore::ROMSColumns srcCols(ms);
    const casacore::ROMSObservationColumns& oc = srcCols.observation();
    const casacore::ROArrayColumn<casacore::Double> times = casacore::ROArrayColumn<casacore::Double>(oc.timeRange());

    // Get the position
    const casacore::ROMSAntennaColumns& ac = srcCols.antenna();
    const casacore::ROArrayColumn<casacore::Double> ants = casacore::ROArrayColumn<casacore::Double>(ac.position());

    // Get the direction
    casacore::ArrayColumn<casacore::Double> refDir;
    casacore::Array<casacore::Double> refDirValues;
    bool useRefDirection = false;
    if ( parset.isDefined("doppler_direction") ) {
        const std::vector<std::string> dirVector = parset.getStringVector("doppler_direction");
        const casacore::MDirection radec = asMDirection(dirVector);
        refDirValues = radec.getValue().get();
        useRefDirection = true;
        ASKAPLOG_INFO_STR(logger,"Using reference direction from parset - direction given : " << askap::printLon(radec)<<","<<askap::printLat(radec));
    } else {
        const casacore::ROMSFieldColumns& fieldCol = srcCols.field();
        refDir = fieldCol.referenceDir();
        ASKAPLOG_INFO_STR(logger,"Using reference direction from MS");
        //const casacore::ArrayColumn<casacore::Double>& refDir = fieldCol.referenceDir();
    }

    const casacore::ROMSSpWindowColumns& sc = srcCols.spectralWindow();

    const casacore::Int srow = spwId;

    const casacore::uInt nChanIn = endChan - startChan + 1;
    const casacore::uInt nChanOut = nChanIn / width;

    //casacore::rownr_t currentRow = row;
    casacore::rownr_t currentRow = 0;
    casacore::uInt rowInThisIteration = 0;


    // Read the position on Antenna 0
    casacore::Array<casacore::Double> posval;
    ants.get(0,posval,true);
    std::vector<double> pval = posval.tovector();

    casacore::MVPosition mvobs(casacore::Quantity(pval[0], "m").getBaseValue(),
                               casacore::Quantity(pval[1], "m").getBaseValue(),
                               casacore::Quantity(pval[2], "m").getBaseValue());
    casacore::MPosition position(mvobs,casacore::MPosition::ITRF);

    casacore::MVTime dat;
    casacore::MVEpoch mvepoch;
    casacore::MEpoch epoch;

    casacore::MeasFrame frame;
    casacore::Double lastTime = -1.0;

    casacore::MFrequency::Ref refout;
    casacore::MFrequency::Ref refin;

    casacore::MFrequency::Convert forw;
    casacore::MFrequency::Convert reverse;;
    const casacore::uInt thisRef = casacore::ROScalarColumn<casacore::Int>(ms.spectralWindow(),"MEAS_FREQ_REF")(0);

    std::vector<double> chanFreq;
    chanFreq.resize(nChanOut);
    for (casacore::uInt destChan = 0; destChan < nChanOut; ++destChan) {
        chanFreq[destChan] = 0.0;
        const casacore::uInt chanOffset = startChan - 1 + (destChan * width);
        for (casacore::uInt i = chanOffset; i < chanOffset + width; ++i) {
            chanFreq[destChan] += sc.chanFreq()(srow)(casacore::IPosition(1, i));
        }
        // Finally average chanFreq
        chanFreq[destChan] = chanFreq[destChan] / width;
    }
    /// The name here can be confusing. But in fact it is a vector that contains
    /// the bary or lsrk frequencies converted to the TOPO frame
    casacore::Vector<double> topoChanFreq(chanFreq.size());

    while ( rowInThisIteration < nRowsThisIteration ) {

        // Get the time
        casacore::Double timestamp = srcCols.time()(currentRow+row);
        if ( timestamp != lastTime ) {
            double mjd = timestamp/(86400.);
            dat = casacore::MVTime(mjd);
            mvepoch = casacore::MVEpoch(dat.day());
            epoch = casacore::MEpoch(mvepoch);

            casacore::Int fieldId = srcCols.fieldId()(currentRow+row);
            const casacore::Array<casacore::Double> values = (useRefDirection ? refDirValues :  refDir.get(fieldId));
            ASKAPCHECK(values.nelements() == 2, "Expected ref direction contains 2 values, RA and Dec in radians");
            const std::vector<casacore::Double> vect = values.tovector();
            const casacore::Quantity ra(vect[0],"rad");
            const casacore::Quantity dec(vect[1],"rad");
            const casacore::MDirection direction(ra,dec);
            frame = casacore::MeasFrame(epoch,position,direction);

            //refout = casacore::MFrequency::Ref(casacore::MFrequency::castType(thisRef),frame);
            //refin = casacore::MFrequency::Ref(outputRefFrame,frame);
            //forw = casacore::MFrequency::Convert(refout,refin);
            refin = casacore::MFrequency::Ref(casacore::MFrequency::castType(thisRef),frame);
            refout = casacore::MFrequency::Ref(outputRefFrame,frame);
            forw = casacore::MFrequency::Convert(refin,refout);
            reverse = casacore::MFrequency::Convert(refout,refin);

            /// here we use the same frequencies in the chanFreq but assuming that they are in
            // BARY or LSRK frame and we convert them back to TOPO frame.
            for (casacore::uInt chan = 0; chan < chanFreq.size(); ++chan) {
                topoChanFreq(chan) = reverse(chanFreq[chan]).getValue();
            }

            lastTime = timestamp;
        }



        // dataArr is a cube where the last dimension is the number of rows in the slicer
        // thisRowDataMatrix is a complex array of say [4 350]
        casacore::Matrix<casacore::Complex> thisRowDataMatrix = indata[currentRow];
        casacore::Matrix<casacore::Bool> thisRowFlagMatrix = outflags[currentRow];
        //casacore::Matrix<casacore::Bool> thisRowFlagMatrix = outflags[currentRow].copy();

        // convert the frequency from one frame to another (e.g: TOPO to BARY)
        // do the channel shifting/interpolate
        casacore::Array<casacore::Bool> flags;
        casacore::Array<casacore::Complex> outdata;
        // what we got here is :
        // - chanFreq contains the input MS frequencies in TOPO frama. It is passed in
        // as xin argument to casacore 1D interpolate() function
        // - topoChanFreq contains the same frequency values as chanFreq but in the BARY/LSRK
        //   frame which are then converted to TOPO frame. It is passed in as xout argument
        //   to casacore 1D interpolate() function.
        itsChanShift->interpolate(chanFreq,topoChanFreq,thisRowDataMatrix,
                                  thisRowFlagMatrix,outdata,flags);

        outputdata[currentRow] = outdata;
        outflags[currentRow] = flags;

        currentRow += 1;
        rowInThisIteration += 1;
    }
}
