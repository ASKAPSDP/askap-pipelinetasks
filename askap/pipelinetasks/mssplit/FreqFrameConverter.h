/// @file FreqFrameConverter.h
///
/// @copyright (c) 2013 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#ifndef ASKAP_CP_FREQFRAMECONVERTER_H
#define ASKAP_CP_FREQFRAMECONVERTER_H

// Package level header file
#include "askap_pipelinetasks.h"

// Include own header file
#include "askap/pipelinetasks/mssplit/IChannelShift.h"

// System includes
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <memory>

// ASKAPsoft includes
#include "Common/ParameterSet.h"
#include "casacore/ms/MeasurementSets/MeasurementSet.h"

namespace askap {
namespace cp {
namespace pipelinetasks {

/// @brief - This class does the frequency frame correction.
class FreqFrameConverter {
    public:
        /// @brief - Constructor
        /// @details - The constructor uses the parset to determine which
        ///            interpolation method to use. Currently, the method
        ///            supported are (1) nearest neighbour, (2) linear
        ///            and (3) cubic. The default method is (1). The parset 
        ///            parameter to specify which method to use is "interpolation" 
        ///            and the supported values are (1) "nearest", (2) "linear"
        ///            and (3) "cubic"
        FreqFrameConverter(const LOFAR::ParameterSet &parset);

        /// @brief - This method does the doppler correction.
        /// @param[in] parset - the task configuration parameters
        /// @param[in] ms - input measurement set
        /// @param[in] startChan - the start channel          
        /// @param[in] endChan - end channel
        /// @param[in] width - channel width. (width > 1 => channels averaging which is not yet supported)
        /// @param[in] row - current row in the main table
        /// @param[in] nRowsThisIteration - the number of rows to process
        /// @param[in] spwId - spectral window id
        /// @param[in] outputRefFrame - the output frame (i.e BARY or LSRK)
        /// @param[in] indata - the visibilities data of nRowsThisIteration
        /// @param[out] outflags - output MS flag values for nRowsThisIteration
        /// @param[out] outputdata - output MS visibilities data for nRowsThisIteration
        void dopplerShift(const LOFAR::ParameterSet& parset,
                         const casacore::MeasurementSet& ms,
                         const uint32_t startChan,
                         const uint32_t endChan,
                         const uint32_t width,
                         const casacore::rownr_t row,
                         const casacore::uInt nRowsThisIteration,
                         const casacore::Int spwId,
                         const casacore::MFrequency::Types outputRefFrame,
                         const casacore::Array<casacore::Complex>& indata,
                         casacore::Cube<casacore::Bool>& outflags,
                         casacore::Cube<casacore::Complex>& outputdata);

    private:
        /// @brief an object that does the actual doppler correction
        std::unique_ptr<IChannelShift> itsChanShift;
};
}
}
}

#endif
