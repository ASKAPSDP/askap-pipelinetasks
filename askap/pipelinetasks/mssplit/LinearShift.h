/// @file LinearShift.h
///
/// @copyright (c) 2013 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#ifndef ASKAP_CP_LINEARSHIFT_H
#define ASKAP_CP_LINEARSHIFT_H


#include "askap/pipelinetasks/mssplit/IChannelShift.h"

namespace askap {
namespace cp {
namespace pipelinetasks {

/// @brief - This class implements the interpolate() method using the linear interpolation
class LinearShift : public IChannelShift
{
    public:
        /// @see IChannelShift::interpolate() for more details.
        void interpolate(const std::vector<double>& chanFreq, 
                    const casacore::Vector<double>& baryChanFreq,
                    const casacore::Array<casacore::Complex>& data,
                    const casacore::Array<casacore::Bool>& inflags,
                    casacore::Array<casacore::Complex>& outdata,
                    casacore::Array<casacore::Bool>& flags) override;
};

}
}
}
#endif
