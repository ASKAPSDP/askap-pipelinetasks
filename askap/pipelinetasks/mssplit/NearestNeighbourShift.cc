/// @file NearestNeighbourShift.cc
///
/// @copyright (c) 2013 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Minh Vuong <minh.vuong@csiro.au>

#include "askap/pipelinetasks/mssplit/NearestNeighbourShift.h"

// ASKAPsoft includes
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapLogging.h"

#include "casacore/scimath/Mathematics/InterpolateArray1D.h"
#include "casacore/ms/MeasurementSets/MeasurementSet.h"

ASKAP_LOGGER(logger, ".nearestneighbourshift");

using namespace askap;
using namespace askap::cp::pipelinetasks;
using namespace casacore;

/// @brief - Implements the interpolate() method using the linear interpolation.
/// @see IChannelShift::interpolate() for more details.
void NearestNeighbourShift::interpolate(const std::vector<double> &chanFreq,
                                        const casacore::Vector<double> &baryChanFreq,
                                        const casacore::Array<casacore::Complex> &data,
                                        const casacore::Array<casacore::Bool> &inflags,
                                        casacore::Array<casacore::Complex> &outdata,
                                        casacore::Array<casacore::Bool> &flags)
{
    const casacore::Vector<double> xin{chanFreq};

    casacore::InterpolateArray1D<double, casacore::Complex>::interpolate(outdata, flags, baryChanFreq, xin, data, inflags,
                                                                         casacore::InterpolateArray1D<double, casacore::Complex>::nearestNeighbour);
}
