/// @file CasdaUploadApp.cc
///
/// @copyright (c) 2015 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>

// Include own header file first
#include "askap/pipelinetasks/casdaupload/CasdaUploadApp.h"

// Include package level header file
#include "askap_pipelinetasks.h"

// System includes
#include <string>
#include <vector>

// ASKAPsoft includes
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "Common/ParameterSet.h"
#include "Common/KVpair.h"
#include "askap/askap/StatReporter.h"
#include "boost/scoped_ptr.hpp"
#include "boost/filesystem.hpp"
#include "xercesc/dom/DOM.hpp" // Includes all DOM
#include "xercesc/framework/LocalFileFormatTarget.hpp"
#include "xercesc/framework/XMLFormatter.hpp"
#include "askap/votable/XercescString.h"
#include "askap/votable/XercescUtils.h"

// Local package includes
#include "askap/pipelinetasks/casdaupload/IdentityElement.h"
#include "askap/pipelinetasks/casdaupload/ObservationElement.h"
#include "askap/pipelinetasks/casdaupload/ImageElement.h"
#include "askap/pipelinetasks/casdaupload/CatalogueElement.h"
#include "askap/pipelinetasks/casdaupload/MeasurementSetElement.h"
#include "askap/pipelinetasks/casdaupload/UVgridElement.h"
#include "askap/pipelinetasks/casdaupload/EvaluationReportElement.h"
#include "askap/pipelinetasks/casdaupload/CasdaChecksumFile.h"
#include "askap/pipelinetasks/casdaupload/CasdaFileUtils.h"

// Using
using namespace std;
using namespace askap::cp::pipelinetasks;
using namespace xercesc;
using askap::accessors::XercescString;
namespace fs = boost::filesystem;

ASKAP_LOGGER(logger, ".CasdaUploadApp");

int CasdaUploadApp::run(int argc, char* argv[])
{
    try {

        ASKAPLOG_INFO_STR(logger, "CASDA upload utility " << ASKAP_PACKAGE_VERSION);

        StatReporter stats;

        itsParset = config();
        checkParset();

        const IdentityElement identity(itsParset);

        const vector<ImageElement> images(
            buildArtifactElements<ImageElement>("images.artifactlist"));
        const vector<CatalogueElement> catalogues(
            buildArtifactElements<CatalogueElement>("catalogues.artifactlist"));
        const vector<MeasurementSetElement> ms(
            buildArtifactElements<MeasurementSetElement>("measurementsets.artifactlist"));
        const vector<UVgridElement> uvgrids(
            buildArtifactElements<UVgridElement>("uvgrids.artifactlist"));
        const vector<EvaluationReportElement> reports(
            buildArtifactElements<EvaluationReportElement>("evaluation.artifactlist"));

        if (images.empty() && catalogues.empty() && ms.empty() && uvgrids.empty() && reports.empty()) {
            ASKAPTHROW(AskapError, "No artifacts declared for upload");
        }

        // If a measurement set is present, we can determine the time range for the
        // observation. Note, only the first measurement set (if there are multiple)
        // is used in this calculation.
        ObservationElement obs;
        if (!ms.empty()) {
            if (ms.size() > 1) {
                ASKAPLOG_WARN_STR(logger, "Multiple measurement sets were specified."
                                  << " Only the first one will be used to populate the"
                                  << " observation metadata");
            }
            const MeasurementSetElement& firstMs = ms[0];
            obs.setObsTimeRange(firstMs.getObsStart(), firstMs.getObsEnd());
        } else if (!uvgrids.empty()) {
            if (uvgrids.size() > 1) {
                ASKAPLOG_WARN_STR(logger, "Multiple UV grids were specified."
                                  << " Only the first one will be used to populate the"
                                  << " observation metadata");
            }
            const UVgridElement& firstUVgrid = uvgrids[0];
            obs.setObsTimeRange(firstUVgrid.getObsStart(), firstUVgrid.getObsEnd());

        } else {
            casacore::MEpoch start, end;

            if (itsParset.isDefined("obsStart")) {
                std::string obsStart = itsParset.getString("obsStart");
                casacore::Quantity qStart;
                casacore::MVTime::read(qStart, obsStart);
                start = casacore::MEpoch(qStart);
            } else {
                ASKAPTHROW(AskapError, "Unknown observation start time - please use \"obsStart\" to specify the start time in the absence of measurement sets.");
            }
            if (itsParset.isDefined("obsEnd")) {
                std::string obsEnd = itsParset.getString("obsEnd");
                casacore::Quantity qEnd;
                casacore::MVTime::read(qEnd, obsEnd);
                end = casacore::MEpoch(qEnd);
            } else {
                ASKAPTHROW(AskapError, "Unknown observation end time - please use \"obsStart\" to specify the end time in the absence of measurement sets.");
            }
            obs.setObsTimeRange(start, end);
        }

        // Create the output directory
        const fs::path outbase(itsParset.getString("outputdir"));
        if (!is_directory(outbase)) {
            ASKAPTHROW(AskapError, "Directory " << outbase
                       << " does not exist or is not a directory");
        }
        const fs::path outdir = outbase / itsParset.getString("sbid");
        ASKAPLOG_INFO_STR(logger, "Using output directory: " << outdir);
        if (!is_directory(outdir)) {
            create_directory(outdir);
        }
        if (!exists(outdir)) {
            ASKAPTHROW(AskapError, "Failed to create directory " << outdir);
        }
        permissions(outdir, boost::filesystem::add_perms | boost::filesystem::group_write);
        const fs::path metadataFile = outdir / "observation.xml";
        generateMetadataFile(metadataFile, identity, obs, images, catalogues, ms, uvgrids, reports);
        CasdaFileUtils::checksumFile(metadataFile);

        // Tar up measurement sets
        bool useAbsolutePath = itsParset.getBool("useAbsolutePaths","true");
        for (vector<MeasurementSetElement>::const_iterator it = ms.begin();
             it != ms.end(); ++it) {
            const fs::path in(it->getFilepath());
            fs::path out;
            if (useAbsolutePath){
                if (in.string()[0] != '/') {
                    out = (boost::filesystem::current_path() / in);
                } else {
                    out = in;
                }
            } else {
                out = (outdir / in.filename());
            }
            out += ".tar";
            ASKAPLOG_INFO_STR(logger, "Tarring file " << in << " to " << out);
            CasdaFileUtils::tarAndChecksum(in, out, itsParset.getBool("clobberTarfile",false));
        }

        // Copy artifacts and checksum
        copyAndChecksumElements<ImageElement>(images, outdir);
        copyAndChecksumElements<CatalogueElement>(catalogues, outdir);
        copyAndChecksumElements<UVgridElement>(uvgrids, outdir); // assume these are already tarred up
        copyAndChecksumElements<EvaluationReportElement>(reports, outdir);

        // Finally, and specifically as the last step, write the READY file
        // For now, this is only done if the config file specifically
        // requests it via the writeREADYfile parameter.
        if (itsParset.getBool("writeREADYfile", false)) {
            const fs::path readyFilename = outdir / "READY";
            CasdaFileUtils::writeReadyFile(readyFilename);
        }

        stats.logSummary();
        ///==============================================================================
    } catch (const askap::AskapError& x) {
        ASKAPLOG_FATAL_STR(logger, "Askap error in " << argv[0] << ": " << x.what());
        std::cerr << "Askap error in " << argv[0] << ": " << x.what() << std::endl;
        exit(1);
    } catch (const std::exception& x) {
        ASKAPLOG_FATAL_STR(logger,
                           "Unexpected exception in " << argv[0] << ": " << x.what());
        std::cerr << "Unexpected exception in " << argv[0] << ": " <<
            x.what() << std::endl;
        exit(1);
    }

    return 0;
}

void CasdaUploadApp::generateMetadataFile(
    const fs::path& file,
    const IdentityElement& identity,
    const ObservationElement& obs,
    const std::vector<ImageElement>& images,
    const std::vector<CatalogueElement>& catalogues,
    const std::vector<MeasurementSetElement>& ms,
    const std::vector<UVgridElement>& uvgrids,
    const std::vector<EvaluationReportElement>& reports)
{
    xercesc::XMLPlatformUtils::Initialize();

    boost::scoped_ptr<LocalFileFormatTarget> target(new LocalFileFormatTarget(
                XercescString(file.string())));

    // Create document
    DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(XercescString("LS"));
    DOMDocument* doc = impl->createDocument();
    doc->setXmlVersion(XercescString("1.0"));
    doc->setXmlStandalone(true);

    // Create the root element and add it to the document
    DOMElement* root = doc->createElement(XercescString("dataset"));
    root->setAttributeNS(XercescString("http://www.w3.org/2000/xmlns/"),
                         XercescString("xmlns"), XercescString("http://au.csiro/askap/observation"));
    doc->appendChild(root);

    // Add identity element
    root->appendChild(identity.toXmlElement(*doc));

    // Add observation element
    root->appendChild(obs.toXmlElement(*doc));

    // Create artifact elements
    appendElementCollection<ImageElement>(images, "images", root);
    appendElementCollection<CatalogueElement>(catalogues, "catalogues", root);
    appendElementCollection<MeasurementSetElement>(ms, "measurement_sets", root);
    appendElementCollection<UVgridElement>(uvgrids, "measurement_sets", root);  //UVgrids don't have their own section - put in MS section for now.
    appendElementCollection<EvaluationReportElement>(reports, "evaluations", root);

    // Write
    DOMLSSerializer* writer = ((DOMImplementationLS*)impl)->createLSSerializer();

    if (writer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true)) {
        writer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
    }

    DOMLSOutput* output = ((DOMImplementationLS*)impl)->createLSOutput();
    output->setByteStream(target.get());
    writer->write(doc, output);

    // Cleanup
    output->release();
    writer->release();
    doc->release();
    target.reset(0);
    xercesc::XMLPlatformUtils::Terminate();
}

template <typename T>
std::vector<T> CasdaUploadApp::buildArtifactElements(const std::string& key) const
{
    vector<T> elements;
    bool useAbsolutePath = itsParset.getBool("useAbsolutePaths","true");
    
    if (itsParset.isDefined(key)) {
        const vector<string> names = itsParset.getStringVector(key);
        for (vector<string>::const_iterator it = names.begin(); it != names.end(); ++it) {
            LOFAR::ParameterSet subset = itsParset.makeSubset(*it + ".");
            subset.replace("artifactparam", *it);
            subset.replace(LOFAR::KVpair("useAbsolutePaths", useAbsolutePath));
            elements.push_back(T(subset));
        }
    }

    return elements;
}

template <typename T>
void CasdaUploadApp::appendElementCollection(const std::vector<T>& elements,
        const std::string& tag,
        xercesc::DOMElement* root)
{
    // Create measurement set elements
    if (!elements.empty()) {
        DOMDocument* doc = root->getOwnerDocument();
        DOMElement *child;
        DOMNodeList *nodelist = doc->getElementsByTagName(XercescString(tag));
        if (nodelist->getLength() > 0) {
            child = (DOMElement *)(nodelist->item(0));
        } else {
            child = doc->createElement(XercescString(tag));
        }
        for (typename vector<T>::const_iterator it = elements.begin();
                it != elements.end(); ++it) {
            child->appendChild(it->toXmlElement(*doc));
        }
        root->appendChild(child);
    }
}

template <typename T>
void CasdaUploadApp::copyAndChecksumElements(const std::vector<T>& elements,
        const fs::path& outdir)
{
    for (typename vector<T>::const_iterator it = elements.begin();
            it != elements.end(); ++it) {
        it->copyAndChecksum(outdir);
    }
}

void CasdaUploadApp::checkParset()
{
    std::string listnames[4] = {"images", "catalogues", "measurementsets", "evaluation"};
    for (int i = 0; i < 4; i++) {
        if ((itsParset.isDefined(listnames[i] + ".artefactlist")) &&
                (!itsParset.isDefined(listnames[i] + ".artifactlist"))) {

            ASKAPLOG_WARN_STR(logger, "You have defined " << listnames[i] <<
                              ".artefactlist instead of " << listnames[i] <<
                              ".artifactlist. Replacing for now, but CHANGE YOUR PARSET!");

            itsParset.add(listnames[i] + ".artifactlist",
                          itsParset.get(listnames[i] + ".artefactlist"));

        }
    }

}
