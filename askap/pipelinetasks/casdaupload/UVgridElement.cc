/// @file UVgridElement.cc
///
/// @copyright (c) 2015 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>

// Include own header file first
#include "askap/pipelinetasks/casdaupload/UVgridElement.h"

// Include package level header file
#include "askap_pipelinetasks.h"

// System includes
#include <string>
#include <vector>

// ASKAPsoft includes
#include "askap/askap/AskapLogging.h"
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapUtil.h"
#include "xercesc/dom/DOM.hpp" // Includes all DOM
#include "boost/filesystem.hpp"
#include "askap/votable/XercescString.h"
#include "askap/votable/XercescUtils.h"

// Local package includes
#include "askap/pipelinetasks/casdaupload/MeasurementSetElement.h"
#include "askap/pipelinetasks/casdaupload/ScanElement.h"

// Using
using namespace std;
using namespace askap::cp::pipelinetasks;
using namespace casacore;
using xercesc::DOMElement;
using askap::accessors::XercescString;
using askap::accessors::XercescUtils;

ASKAP_LOGGER(logger, ".UVgridElement");

UVgridElement::UVgridElement(const LOFAR::ParameterSet &parset)
    : ProjectElementBase(parset)
{
    itsName = "measurement_set";  // Needs to fit in the measurement_sets section of the XML file
    itsFormat = "tar";
    if (parset.isDefined("ms")) {
        itsMSpath = parset.getString("ms");
    } else {
        ASKAPTHROW(AskapError,
                   "MS is not defined for UV grid " << itsFilepath.string());
    }
    LOFAR::ParameterSet msparset;
    msparset.add("filename",itsMSpath.string());
    msparset.add("project", itsProject);
    MeasurementSetElement ms(msparset);

    itsScans = ms.scans();
}

xercesc::DOMElement* UVgridElement::toXmlElement(xercesc::DOMDocument& doc) const
{
    // Have to break the inheritence pattern, as we need to append
    // '.tar' onto the filename path so that the entry in the
    // observation.xml file matches what is on disk.
    DOMElement* e = doc.createElement(XercescString(itsName));

    if (itsUseAbsolutePaths) {
        std::string path = itsFilepath.string();
        if (path[0] != '/') {
            path = boost::filesystem::current_path().string() + "/" + path;
        }
        XercescUtils::addTextElement(*e, "filename", path);
    } else {
        XercescUtils::addTextElement(*e, "filename", itsFilepath.filename().string());
    }
    XercescUtils::addTextElement(*e, "format", itsFormat);
    XercescUtils::addTextElement(*e, "project", itsProject);

    // Confirm that there is at least one scan element
    // Throw an error if not
    ASKAPCHECK(itsScans.size() > 0,
               "No scans are present in the UV grid" << itsFilepath);

    // Create scan elements
    DOMElement* child = doc.createElement(XercescString("scans"));
    for (vector<ScanElement>::const_iterator it = itsScans.begin();
            it != itsScans.end(); ++it) {
        child->appendChild(it->toXmlElement(doc));
    }
    e->appendChild(child);
    return e;
}

casacore::MEpoch UVgridElement::getObsStart(void) const
{
    return itsObsStart;
}

casacore::MEpoch UVgridElement::getObsEnd(void) const
{
    return itsObsEnd;
}
