cmake_minimum_required (VERSION 3.12.0)

option(USE_SMS "build with sky model" OFF)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

message(STATUS "CMAKE_CXX_COMPILER = ${CMAKE_CXX_COMPILER}")
set(VERSION "1.18.1")
project(askap-pipelinetasks
        VERSION ${VERSION}
        DESCRIPTION "ASKAP Pipelinetasks"
        LANGUAGES CXX C)

if (USE_SMS)
    add_definitions(-DUSE_SMS)
endif()

find_package(Git REQUIRED)

include(CheckCXXCompilerFlag)

include (GNUInstallDirs)
include(dependencies.cmake)

# Load any developer overrides for FetchContent behaviour like external sources, feature branch inclusion, non default version inclusion, etc.
# DEV_OVERRIDES should be a full path to a cmake script.
# See https://cmake.org/cmake/help/latest/module/FetchContent.html for finer details.
message (STATUS "DEV_OVERRIDES = $ENV{DEV_OVERRIDES}")
if(DEFINED ENV{DEV_OVERRIDES})
  message (STATUS "cmake config overrides file specified, $ENV{DEV_OVERRIDES}")
  include($ENV{DEV_OVERRIDES} OPTIONAL)
else()
  message (STATUS "No cmake config overrides file specified, using defaults")
endif()

message (STATUS "Forming build string")
execute_process(
   COMMAND git describe --tags --always --dirty
   OUTPUT_VARIABLE BUILD_STR
   OUTPUT_STRIP_TRAILING_WHITESPACE
)
message(STATUS "Build string is ${BUILD_STR}")

include(FetchContent)

# -------- askap cmake modules --------
FetchContent_Declare(askap-cmake
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/askap-cmake.git
  GIT_TAG           ${ASKAP_CMAKE_TAG}
  SOURCE_DIR        askap-cmake
)

# -------- lofar common --------
FetchContent_Declare(lofar-common
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/lofar-common.git
  GIT_TAG           ${LOFAR_COMMON_TAG}
  SOURCE_DIR        lofar-common
)

# -------- lofar blob --------
FetchContent_Declare(lofar-blob
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/lofar-blob.git
  GIT_TAG           ${LOFAR_BLOB_TAG}
  SOURCE_DIR        lofar-blob
)

# -------- base-askap --------
FetchContent_Declare(base-askap
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-askap.git
  GIT_TAG           ${BASE_ASKAP_TAG}
  SOURCE_DIR        base-askap
)

# -------- base-logfilters --------
FetchContent_Declare(base-logfilters
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-logfilters.git
  GIT_TAG           ${BASE_LOGFILTERS_TAG}
  SOURCE_DIR        base-logfilters
)

# -------- base-imagemath --------
FetchContent_Declare(base-imagemath
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-imagemath.git
  GIT_TAG           ${BASE_IMAGEMATH_TAG}
  SOURCE_DIR        base-imagemath
)

# -------- base-askapparallel --------
FetchContent_Declare(base-askapparallel
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-askapparallel.git
  GIT_TAG           ${BASE_ASKAPPARALLEL_TAG}
  SOURCE_DIR        base-askapparallel
)

# -------- base-scimath --------
FetchContent_Declare(base-scimath
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-scimath.git
  GIT_TAG           ${BASE_SCIMATH_TAG}
  SOURCE_DIR        base-scimath
)

# -------- base-accessors --------
FetchContent_Declare(base-accessors
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-accessors.git
  GIT_TAG           ${BASE_ACCESSORS_TAG}
  SOURCE_DIR        base-accessors
)

# -------- base-components --------
FetchContent_Declare(base-components
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-components.git
  GIT_TAG           ${BASE_COMPONENTS_TAG}
  SOURCE_DIR        base-components
)

# -------- askap-analysis --------
FetchContent_Declare(askap-analysis
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/askap-analysis.git
  GIT_TAG           ${ASKAP_ANALYSIS_TAG}
  SOURCE_DIR        askap-analysis
)

# -------- yandasoft --------
FetchContent_Declare(yandasoft
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/yandasoft.git
  GIT_TAG           ${ASKAP_YANDASOFT_TAG}
  SOURCE_DIR        yandasoft
)

# -------- askap-interfaces --------
FetchContent_Declare(askap-interfaces
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/askap-interfaces.git
  GIT_TAG           ${ASKAP_INTERFACES_TAG}
  SOURCE_DIR        askap-interfaces
)

# -------- askap-sms --------
FetchContent_Declare(askap-sms
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/askap-sms.git
  GIT_TAG           ${ASKAP_SMS_TAG}
  SOURCE_DIR        askap-sms
)

# Put our askap cmake modules in the cmake path
# We need this before any other Fetch calls so we have the find_package routines available
message (STATUS "Fetching askap-cmake files")
FetchContent_GetProperties(askap-cmake)
if(NOT askap-cmake_POPULATED)
  FetchContent_Populate(askap-cmake)

  list(APPEND CMAKE_MODULE_PATH "${askap-cmake_SOURCE_DIR}")
endif()
message(STATUS "askap-cmake path is ${askap-cmake_SOURCE_DIR}")

# Load the sub repos, this results in git operations during configure time
# MakeAvailable performs an "add_subdirectory" call after doing the fetch/update
message (STATUS "Fetching sub repos")
FetchContent_MakeAvailable(lofar-common lofar-blob
                           base-askap base-logfilters base-imagemath base-askapparallel base-scimath
                           base-accessors base-components askap-analysis yandasoft 
                           askap-interfaces askap-sms
)

message (STATUS "Done - Fetching sub repos")

option (CXX11 "Compile as C++11 if possible" NO)
option (ENABLE_SHARED "Build shared libraries" YES)
option (ENABLE_RPATH "Include rpath in executables and shared libraries" YES)

# uninstall target
if(NOT TARGET uninstall)
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
        IMMEDIATE @ONLY)

    add_custom_target(uninstall
        COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif()

if (ENABLE_SHARED)
    option (BUILD_SHARED_LIBS "" YES)
    if (ENABLE_RPATH)
        # Set RPATH to use for installed targets; append linker search path

        # On galaxy and ingest service node, this is all it needs to work
        set(CMAKE_INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib" )
        set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
        if ( APPLE )
            # on OSX, the app/executable may not run because it can not find
            # the shared libraries (e.g: ICE and/or log4cxx libraries etc).
            # At least this is what I saw on my MacOS laptop when I tried to
            # get this repo to build and run. I could be wrong but I assume 
            # this is because of the way I installed ICE and log4cxx on my laptop.
            # I got the old version of log4cxx (0.11.0) and Ice (3.6) while
            # brew install will install log4cxx (0.12.0) and Ice (3.7) which are
            # both dont work with askapsoft.
            list(APPEND CMAKE_INSTALL_RPATH "${log4cxx_LIBRARY}" "${ICE_LIB}")
        endif()

        set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    endif (ENABLE_RPATH)
endif(ENABLE_SHARED)

# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
	"${PROJECT_SOURCE_DIR}/askap_pipelinetasks.h.in"
	"${PROJECT_SOURCE_DIR}/askap_pipelinetasks.h"
)
configure_file (
	"${PROJECT_SOURCE_DIR}/askap_pipelinetasks.cc.in"
    "${PROJECT_SOURCE_DIR}/askap_pipelinetasks.cc"
)



# find packages
find_package(Boost REQUIRED COMPONENTS regex system filesystem)

# Dont build casdaupload if we are building on MacOS. The reason is it requires
# openssl to be installed which in turn other packages (e.g: log4cxx 0.12.0 from memory)
# that break askap build. AT least, this is my experience when it tried to build it.
if ( NOT APPLE )
    find_package(OpenSSL)
endif()

find_package(CPPUnit)

# include directories
include_directories( ${CMAKE_SOURCE_DIR} )
include_directories (${Boost_INCLUDE_DIRS})
include_directories(${log4cxx_INCLUDE_DIRS})
include_directories(${CASACORE_INCLUDE_DIRS})
include_directories(${COMPONENTS_INCLUDE_DIRS})
include_directories(${MPI_INCLUDE_PATH})
include_directories(${XercesC_INCLUDE_DIRS})

if (CXX11)
	set(CMAKE_CXX_STANDARD 11)
	set(CMAKE_CXX_STANDARD_REQUIRED ON)
endif()

add_library(pipelinetasks SHARED)

if ( APPLE )
    set(SMSCLIENT_LIB libaskap-smsclient.dylib)
    if ( USE_SMS )
        set (ICE_LIBRARIES ${ICE_LIB}/libIceDiscovery.dylib ${ICE_LIB}/libIce.dylib)
    endif()
else()
    set(SMSCLIENT_LIB libaskap-smsclient.so)
    if ( USE_SMS )
        set (ICE_LIBRARIES ${ICE_LIB}/libIceDiscovery.so ${ICE_LIB}/libIce.so)
    endif()
endif()

macro(add_sources_to_pipelinetasks)
    foreach(arg IN ITEMS ${ARGN})
        target_sources(pipelinetasks PRIVATE ${CMAKE_CURRENT_LIST_DIR}/${arg})
    endforeach()
endmacro()
target_compile_definitions(pipelinetasks PUBLIC
        casa=casacore
        HAVE_AIPSPP
        HAVE_BOOST
        HAVE_LOG4CXX
)

add_sources_to_pipelinetasks(
    askap_pipelinetasks.cc
)

set_target_properties(pipelinetasks PROPERTIES
        OUTPUT_NAME askap_pipelinetasks
)


add_subdirectory(askap/pipelinetasks/cflag)
add_subdirectory(askap/pipelinetasks/mssplit)
add_subdirectory(askap/pipelinetasks/cmodel)



if (OPENSSL_FOUND)
  add_subdirectory(askap/pipelinetasks/casdaupload)
  include_directories(${OPENSSL_INCLUDE_DIR})
endif ()


# askap-askapsoft defines its project name after all the repos are fetched
# so repo that relies on PROJECT_SOURCE_DIR may have null value
target_include_directories(pipelinetasks PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include>
# skymodel include
  $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/askap-sms/skymodel/client-cpp/smsclient/ice-generated-cpp>
  $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/askap-sms/skymodel/client-cpp/smsclient>
  $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/askap-sms/skymodel/client-cpp>
  $<BUILD_INTERFACE:${FETCHCONTENT_SOURCE_DIR_ASKAP-SMS}/skymodel/client-cpp/smsclient/ice-generated-cpp>
  $<BUILD_INTERFACE:${FETCHCONTENT_SOURCE_DIR_ASKAP-SMS}/skymodel/client-cpp/smsclient>
  $<BUILD_INTERFACE:${FETCHCONTENT_SOURCE_DIR_ASKAP-SMS}/skymodel/client-cpp>
  $<INSTALL_INTERFACE:include/SkyModel>
  ${Boost_INCLUDE_DIRS}
  ${log4cxx_INCLUDE_DIRS}
  ${CASACORE_INCLUDE_DIRS}
  ${XercesC_INCLUDE_DIRS}
  ${ICE_INC}
)

add_dependencies(pipelinetasks libaskap-smsclient)

target_link_libraries(pipelinetasks PUBLIC
		askap::askap
		lofar::Common
        askap::components
        askap::accessors
        $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/_deps/askap-sms-build/skymodel/client-cpp/smsclient/${SMSCLIENT_LIB}>
		${log4cxx_LIBRARY}
		${CASACORE_LIBRARIES}
        ${Boost_LIBRARIES}
		${XercesC_LIBRARY}
        ${ICE_LIBRARIES}
)

if (OPENSSL_FOUND)
target_link_libraries(pipelinetasks PUBLIC
        ${XercesC_LIBRARY}
        ${OPENSSL_SSL_LIBRARY}
        ${OPENSSL_CRYPTO_LIBRARY}
)
endif (OPENSSL_FOUND)


if (MPI_FOUND)
	target_link_libraries(pipelinetasks PUBLIC
		${MPI_LIBRARIES}
	)
endif (MPI_FOUND)

add_library(askap::pipelinetasks ALIAS pipelinetasks)
install (
TARGETS pipelinetasks
EXPORT askap-pipelinetasks-targets
RUNTIME DESTINATION bin
LIBRARY DESTINATION lib
ARCHIVE DESTINATION lib
LIBRARY PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
)



add_subdirectory(askap/pipelinetasks/apps)

if (CPPUNIT_FOUND)  
  include_directories(${CPPUNIT_INCLUDE_DIR})
  include(CTest)
  enable_testing()

  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/tests/cflag)
  add_subdirectory(tests/cflag)

  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/tests/cmodel)
  add_subdirectory(tests/cmodel)

  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/tests/mssplit)
  add_subdirectory(tests/mssplit)

endif (CPPUNIT_FOUND)

# build tDataAccessor
message(STATUS "CMAKE_CXX_COMPILER = ${CMAKE_CXX_COMPILER}")
